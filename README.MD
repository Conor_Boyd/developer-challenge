# The Travelling Salesman Problem

## Challenge

Find the shortest path between each of the towns listed in the `paths.json` 

The `paths.json` gives you a list of the towns as well as distances between each of the towns.

e.g.

```
{
    "towns": [
        "Town A",
        "Town B",
        "Town C"
    ],
    "distances": {
        "Town A": { /* DISTANCE FROM */
            "Town B": 54, /* DISTANCE TO */
            "Town C": 21 /* DISTANCE TO */
        },
        "Town B": { /* DISTANCE FROM */
            "Town A": 54, /* DISTANCE TO */
            "Town C": 88 /* DISTANCE TO */
        },
        "Town C": { /* DISTANCE FROM */
            "Town B": 88, /* DISTANCE TO */
            "Town C": 21 /* DISTANCE TO */
        }
    }
}
```

## Objectives

Write some simple code to:
- Find the shortest path (it doesn't have to be 100% accurate)
- Scale with more towns
- Execution time is the most important factor

## Running 

You can run with:
```
# For PHP
docker-compose run php

# For Javascript
docker-compose run js
```

You can use which ever programming languages you like as long as you can containerise it.

## Submit a solution

Fork this repository and create a pull request