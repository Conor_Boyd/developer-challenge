<?php
//No idea if this is working, based off the Nearest Neighbour Algorithm.

$paths = json_decode(file_get_contents('/tmp/paths.json'));
$towns = $paths->towns;
$distances = $paths->distances;

$route = [];
$distance = 0;

$startingPoint = rand(0, count($towns) - 1);
$startingTown = $towns[$startingPoint];
$previousTown = null;

addTown($paths, $route, $towns, $distance, $previousTown, $startingPoint, 0);

while ($towns) {
    $nearestTown = null;
    $previousDistance = null;
    foreach ($towns as $index=>$town) {
        $currentDistance = $distances->$town->$previousTown;
        if (!$previousDistance || $currentDistance < $previousDistance) {
            $nearestTown = $index;
            $previousDistance = $currentDistance;
        }
    }
    addTown($paths, $route, $towns, $distance, $previousTown, $nearestTown, $previousDistance);
}

addTown($paths, $route, $towns, $distance, $previousTown, $startingPoint, $distances->$previousTown->$startingTown);

echo "Route: " . implode(', ', $route);
echo "\n";
echo "Distance: " . $distance;

function addTown($paths, &$route, &$towns, &$distance, &$previousTown, $townIndex, $townDistance) {
    $town = $paths->towns[$townIndex];
    array_push($route, $town);
    $previousTown = $town;
    $distance += $townDistance;
    unset($towns[$townIndex]);
}
